import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// - Timer should display minutes and seconds in text
// - Timer should count down seconds until it reaches 00:00
// - Phone should buzz when timer reaches 0
// - Timers should switch between 25 and 5 minutes
// - Timer should be able to start, stop, and reset

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
